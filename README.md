# ICM20948 Driver

SPI driver for the [ICM20948](https://invensense.tdk.com/products/motion-tracking/9-axis/icm-20948/) 9-axis IMU
from TDK InvenSense. 

## Requirements
This driver requires the STM32G4 Hardware Abstraction Layer in order to function properly. As it was written to run on
an STM32G431 MCU

## Limitations

**Note,** this driver does not currently support all of the available endpoint functions of the ICM20948.

